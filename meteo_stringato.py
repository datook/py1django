# Chiede una città e ti dice il meteo
import requests

API_KEY = "2c353b8c110db8fe8eca5381b4c9635b"
API_ADDRESS = "http://api.openweathermap.org/data/2.5/weather?appid={}&lang=it&q=".format(API_KEY)

citta = input("Città: ")
url = API_ADDRESS + citta
dati_json = requests.get(url).json()
situazione_meteo = dati_json['weather'][0]['description'] if 'weather' in dati_json else "Non trovato"
print(situazione_meteo)